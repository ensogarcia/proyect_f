package com.mygdx.game.Map;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.maps.Map;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.scenes.scene2d.Actor;

import java.util.Random;

public class ConstructMap extends Actor {
    private Texture wall;
    private Body body;
    public int [][] map_pos;
    int size_mX;
    int size_mY;
    public int auxX;
    public int auxY;
    Random rnd= new Random();
    int[][] dir2 ={ { 0, 1 }, { 1, 0 }, { 0, -1 }, { 1, 1 }, { 1, -1 } };
    int[][] dir1 ={{ 1, 0 }, { 0, 1 }, { -1, 0 }, { -1, 1 }, { 1, 1 }};

    public int pos1X=0;
    public int pos1Y=0;

    public ConstructMap(int size_x,int size_y, int roads)
    {
        size_mX=size_x;
        size_mY=size_y;
        auxX = 2 * size_x + 1;
        auxY = 2 * size_y + 1;
        map_pos=new int[auxX][auxY];
        construct();

        for (int i = 0; i < 9; i++)
        {
            path();
        }
        path2();
        rev();
        Base();

    }

    private void construct() {
        int id = 0;
        for (int i = 0; i < auxX; i++)
        {
            for (int j = 0; j < auxY; j++)
            {
                map_pos[i][j] = (i % 2 == 1 && j % 2 == 1) ? id++ : -1;
            }
        }
    }


    public void Base() {


        if (pos1X < size_mX)
        {
            map_pos[pos1X * 2 + 1 +1][pos1Y * 2 + 1 ] = -8;
            map_pos[pos1X * 2 + 1 +1][pos1Y * 2 + 1 - 1] = -8;
        }

        if (pos1X > 0)
        {
            map_pos[pos1X * 2 + 1 -1][pos1Y * 2 + 1 ] = -8;
            map_pos[pos1X * 2 + 1 -1][pos1Y * 2 + 1 - 1] = -8;
        }

        //map_pos[pos1X * 2 + 1 ][pos1Y * 2 + 1 -1] = -8;

    }

    public void path2()
    {
        int x = rnd.nextInt(size_mX);

        int y = 0;
        int index = -2;

        while (y < size_mY - 1)
        {
            int d = rnd.nextInt(5);

            int dx = x + dir1[d][0];
            int dy = y + dir1[d][1];

            if ((dy < size_mY) && (dx < size_mX && dx >= 0))
            {
                map_pos[dx * 2 + 1][ dy * 2 + 1] = index;
                map_pos[(dx * 2 + 1) - dir1[d][ 0]][ (dy * 2 + 1) - dir1[d][ 1]] = index;

                if (d == 3)
                {
                    map_pos[dx * 2 + 1+1][ (dy * 2 + 1) ] = index;
                    map_pos[(dx * 2 + 1) - dir1[d][ 0]+1][ (dy * 2 + 1) - dir1[d][ 1]] = index;
                }
                else if (d == 4)
                {
                    map_pos[x * 2 + 1+1][ (y * 2 + 1)] = index;
                    map_pos[(dx * 2 + 1) - dir1[d][ 0]+1][ (dy * 2 + 1) - dir1[d][ 1] ] = index;
                }
                x = dx;
                y = dy;
            }
        }

        pos1X=x;
        pos1Y=y;

    }

    public void path()
    {
        int y = rnd.nextInt(size_mY);

        int x = 0;
        int index = -2;

        map_pos[1][ 2 * x + 1] = -4;

        while (x < size_mX - 1)
        {
            int d = rnd.nextInt(5);

            int dx = x + dir2[d][0];
            int dy = y + dir2[d][1];

            if ((dx < size_mX) && (dy < size_mY && dy >= 0))
            {
                map_pos[dx * 2 + 1][ dy * 2 + 1] = index;
                map_pos[(dx * 2 + 1) - dir2[d][ 0]][ (dy * 2 + 1) - dir2[d][ 1]] = index;

                if (d == 3)
                {
                    map_pos[dx * 2 + 1][ (dy * 2 + 1) - 1] = index;
                    map_pos[(dx * 2 + 1) - dir2[d][ 0]][ (dy * 2 + 1) - dir2[d][ 1] - 1] = index;
                }
                else if (d == 4)
                {
                    map_pos[x * 2 + 1][ (y * 2 + 1) - 1] = index;
                    map_pos[(dx * 2 + 1) - dir2[d][ 0]][ (dy * 2 + 1) - dir2[d][ 1] - 1] = index;
                }
                x = dx;
                y = dy;
            }
        }
    }

    public void rev()
    {
        for (int i = 0; i < auxX; i++)
        {
            for (int j = 0; j < auxY; j++)
            {

                if(map_pos[i][ j] >= 0 && map_pos[i][ j+1] ==-1 && map_pos[i+1][ j]==-1 && map_pos[i-1][ j]==-1 && map_pos[i][ j-1]==-1)
                {

                    map_pos[i][ j] = -5;

                }
            }
        }
    }

    @Override
    public void act(float delta) {
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.draw(wall, getX(),getY());
    }
}
