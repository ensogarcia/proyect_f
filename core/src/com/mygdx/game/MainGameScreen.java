package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.mygdx.game.Actors.Character;
import com.mygdx.game.Actors.Controller;
import com.mygdx.game.Map.ConstructMap;
import com.mygdx.game.Map.Wall_p;
import com.mygdx.game.Screens.BaseScreen;

import java.awt.Image;

public class MainGameScreen extends BaseScreen {

    public MainGameScreen(MyGdxGame game) {
        super(game);
    }

    private Stage stage;
    private Character player;
    private Controller Img;
    private Texture textur_player;
    private Texture butt;
    private Texture wall;
    private ConstructMap mapa;

    private Wall_p walls;

    private float tamaño;

    private Texture suelo;
    private SpriteBatch batch= new SpriteBatch();
    //private TextureRegion region_player;
    //private Body minijoeBody;


    @Override
    public void show(){


        Gdx.input.setInputProcessor(stage);
        stage = new Stage();
        stage.setDebugAll(true);








        wall = new Texture("Wall.png");

        suelo= new Texture("suelo.jpg");

        mapa = new ConstructMap(12,7,5);


        for(int i = 0; i < 12*2+1; i++) {
            for(int j = 0; j < 7*2+1; j++) {
                if(mapa.map_pos[i][j]==-1 || mapa.map_pos[i][j]>0 ){
                    walls= new Wall_p(wall);
                    stage.addActor(walls);
                    walls.setPosition(64*i, 64*j);
                }
            }
        }

        //textur_player = new Texture("example.png");
        //player =new Character(textur_player);
        stage.addActor(player);
        player.setPosition(0,0);
        player.setOrigin(player.getWidth()*0.5f, player.getHeight()*0.5f);

        butt = new Texture("butt.png");
        Img = new Controller(butt);
        stage.addActor(Img);
        Img.setPosition(50, 50);

        //player.setSize(500,500);
    }

    @Override
    public void hide(){
        stage.dispose();

    }

    @Override
    public void render(float delta){
       Gdx.gl.glClearColor(0.4f, 0.5f, 0.8f, 1f);
       Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        batch.begin();
        batch.draw(suelo, 0f, 0f,1600,900);

        batch.end();

        stage.act();
        stage.draw();
        player.setSpriteRotation(tamaño);



        if (Gdx.input.isTouched()){
            tamaño=Gdx.input.getX();
            System.out.print(Gdx.input.getX());
            System.out.println();
            System.out.print(Gdx.input.getY());
            System.out.println();
        }

    }


    @Override
    public void dispose() {
        textur_player.dispose();
    }
}
