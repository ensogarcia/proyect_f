package com.mygdx.game.Actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.mygdx.game.Screens.BodyDefFactory;
import com.mygdx.game.Screens.FixtureFactory;

public class Bullet extends Actor {

    private Texture bullet;
    private Sprite sprite;
    public float veloc=10;
    private World world;
    private Body CharacterBody;
    private Fixture CharacterFixture;

    public Bullet(Texture bullet, World world )
    {
        this.bullet=bullet;
        sprite = new Sprite(bullet);
        this.world=world;

        CharacterBody = this.world.createBody(BodyDefFactory.createPlayer());
        CharacterFixture= FixtureFactory.createPlayerFixture(CharacterBody);
        setSize(bullet.getWidth(),bullet.getHeight());
    }

    @Override
    public void act(float delta) {

        Vector2 velocity= new Vector2(0,0);
        float angle=0;

        if (Gdx.input.isKeyPressed(Input.Keys.D)) {
            velocity.x=veloc;
        }

        CharacterBody.setLinearVelocity(velocity);
        CharacterBody.setAngularVelocity(angle);
        sprite.setPosition((40*(CharacterBody.getPosition().x-0.63f))+800, (40*(CharacterBody.getPosition().y-0.5f))+450);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {

        sprite.draw(batch);
    }

    public void setPosition(float x, float y, float angle){
        CharacterBody.setTransform(x,y,angle);
    }

}
