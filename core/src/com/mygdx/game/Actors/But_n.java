package com.mygdx.game.Actors;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.mygdx.game.Screens.Menu;

public class But_n extends Actor  implements ClickEventHandler {

    private Texture button;
    private Sprite sprite;
    private InputListener listener;
    //public final Menu menu;

    public But_n(Texture button,float x,float y,float sx,float sy)
    {
        //this.menu=menu;
        this.button=button;
        sprite = new Sprite(button);
        setBounds(getX(),getY(),button.getWidth(),button.getHeight());

        sprite.setPosition(x,y);
        sprite.setScale(sx,sy);
        listener = new InputListener(){
            public boolean touchDown (InputEvent event, float x, float y, int pointer,int button){
                System.out.println("Click");
                clickhandler.handleClick();
                return true;
            }

            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                System.out.println("DClick");
            }
        };
        addListener(listener);
        setTouchable(Touchable.enabled);
    }

    public void dead(){
        removeListener(listener);
    }

    ClickEventHandler clickhandler;
     public void onClick(ClickEventHandler clickhandler){
         this.clickhandler = clickhandler;
     }

    @Override
    public void act(float delta) {
    }


    @Override
    public void draw(Batch batch, float parentAlpha) {

        sprite.draw(batch);
    }

    @Override
    public void handleClick() {
    }
}

/*
class  ClickHandler implements ClickEventHandler {
    //public void handlerClick
}*/