package com.mygdx.game.Actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.mygdx.game.Screens.BodyDefFactory;
import com.mygdx.game.Screens.FixtureFactory;

public class Character extends Actor {
    private Texture character;
    private Sprite sprite;
    public float veloc=7;
    private World world;
    private Body CharacterBody;
    private Fixture CharacterFixture;

    public Character(Texture character, World world )
    {
        this.character=character;

        sprite = new Sprite(character);

        this.world=world;

        CharacterBody = this.world.createBody(BodyDefFactory.createPlayer());
        CharacterFixture= FixtureFactory.createPlayerFixture(CharacterBody);

        setSize(character.getWidth(),character.getHeight());
    }

    @Override
    public void act(float delta) {

        Vector2 velocity= new Vector2(0,0);
        float angle=0;

        if (Gdx.input.isKeyPressed(Input.Keys.A)) {
            velocity.x=-veloc;
        }
        if (Gdx.input.isKeyPressed(Input.Keys.W)) {
            velocity.y=veloc;
        }
        if (Gdx.input.isKeyPressed(Input.Keys.S)) {
            velocity.y=-veloc;
        }
        if (Gdx.input.isKeyPressed(Input.Keys.D)) {
            velocity.x=veloc;
        }

/*
        if (Gdx.input.isKeyPressed(Input.Keys.Q)) {
            angle=1;
        }
        if (Gdx.input.isKeyPressed(Input.Keys.E)) {
            angle=-1;
        }*/

        CharacterBody.setLinearVelocity(velocity);
        CharacterBody.setAngularVelocity(angle);


       //System.out.println(CharacterBody.getPosition().y + " " + getY());
        sprite.setPosition((40*(CharacterBody.getPosition().x-0.63f))+800, (40*(CharacterBody.getPosition().y-0.5f))+450);
        //sprite.setRotation(angle*90);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {

        sprite.draw(batch);
    }

    public void setSpriteRotation(float angle)
    {
        sprite.setRotation(angle);
    }

    public void setPosition(float x, float y){
        CharacterBody.setTransform(x,y,0);
    }

}
