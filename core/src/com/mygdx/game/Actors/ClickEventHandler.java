package com.mygdx.game.Actors;

public interface ClickEventHandler{
    public void handleClick();
}
