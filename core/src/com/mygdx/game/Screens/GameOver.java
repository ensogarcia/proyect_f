package com.mygdx.game.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.mygdx.game.MyGdxGame;

public class GameOver extends BaseScreen {

    public GameOver(MyGdxGame game) {
        super(game);
        stage= new Stage();

        batch = new SpriteBatch();
        //img = new Texture("playnow.png");
        suelo= new Texture("16.png");

        stage = new Stage(new FitViewport(640, 360));


    }
    private Stage stage;

    SpriteBatch batch;
    //Texture img;
    private Texture suelo;

    private Skin skin;
    private TextButton retry;


    @Override
    public void show() {
        //Gdx.input.setInputProcessor(stage);

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0.9f, 0.85f, 0.85f, 1f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        if (Gdx.input.isKeyPressed(Input.Keys.Z)) {
            game.setScreen(new Menu(game));
        }
        //System.out.print(Gdx.input.getY());
        stage.act();

        batch.begin();
        batch.draw(suelo, 0f, 0f,1600,900);
        //batch.draw(img, 600, 400);
        batch.end();

        stage.draw();
    }
    @Override
    public void hide() {
        // Gdx.input.setInputProcessor(null);

    }

    public void dispose () {
        batch.dispose();
        //img.dispose();
        stage.dispose();
    }
}
