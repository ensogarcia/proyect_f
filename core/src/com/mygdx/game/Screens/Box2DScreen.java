package com.mygdx.game.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.mygdx.game.Actors.Bullet;
import com.mygdx.game.Actors.But_n;
import com.mygdx.game.Actors.Character;
import com.mygdx.game.Actors.ClickEventHandler;
import com.mygdx.game.Map.ConstructMap;
import com.mygdx.game.Map.Wall_p;
import com.mygdx.game.MyGdxGame;
import com.mygdx.game.Screens.BaseScreen;

public class Box2DScreen extends BaseScreen {

    private World world;

    private Box2DDebugRenderer renderer;

    private OrthographicCamera camera;

    private Body CharacterBody, mapBody, ememy;

    private Fixture CharacterFixture, mapFixture;
    //private boolean isAlive = true;
    private Character player;
    private Texture textur_player;
    private ConstructMap mapa;
    private float tamaño;
    private float aux_rot;
    private Stage stage;
    int enem=0;
    private Bullet bullet;
    private Texture textur_bullet;

    private But_n button;
    private Texture Texture_button;


    public Box2DScreen(MyGdxGame game) {

        super(game);
        stage = new Stage();
        world=new World(new Vector2(0,0),true);
        renderer=new Box2DDebugRenderer();
        camera = new OrthographicCamera(16*2.5f, 9*2.5f);
        textur_player = new Texture("example.png");
        player=new Character(textur_player,world);
        mapa = new ConstructMap(16,10,6);

    }



    @Override
    public void show() {





        stage.addActor(player);


        for(int i = 0; i < mapa.auxX; i++) {
            for(int j = 0; j < mapa.auxY; j++) {
                if(mapa.map_pos[i][j]==-1 ){
                    mapBody = world.createBody(BodyDefFactory.createWall(i*1.2f-19.5f,j*1.2f-12f));
                    mapFixture = FixtureFactory.createWallFixture(mapBody);
                }

                if( mapa.map_pos[i][j]>=0)
                {
                    enem++;
                    System.out.print(enem);
                    ememy = world.createBody(BodyDefFactory.createWall(i*1.2f-19.5f,j*1.2f-12f));
                    mapFixture = FixtureFactory.createEnemyFixture(ememy);
                }

                if( mapa.map_pos[i][j]==-8)
                {
                    enem++;
                    System.out.print(enem);
                    ememy = world.createBody(BodyDefFactory.createWall(i*1.2f-19.5f,j*1.2f-12f));
                    mapFixture = FixtureFactory.createEnemyFixture(ememy);
                }
            }
        }

        player.setPosition((mapa.pos1X* 2 + 1)*1.2f-19.5f,(mapa.pos1Y* 2 + 1)*1.2f-12f);

        Gdx.input.setInputProcessor(stage);
        Texture_button=new Texture("butt.png");
        button = new But_n(Texture_button,1400,600,1,1);
        button.onClick(new ClickEventHandler() {
            @Override
            public void handleClick() {

                System.out.println("Disparo");
            }
        });

        stage.addActor(button);
    }

    @Override
    public void dispose() {
        world.dispose();
        renderer.dispose();
    }

    @Override
    public void render(float delta) {

        Gdx.gl.glClearColor(0.4f, 0.5f, 0.8f, 1f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        world.step(delta,6,2);
        camera.update();
        renderer.render(world,camera.combined);

        stage.act();
        stage.draw();
        player.setSpriteRotation(tamaño);

        if (Gdx.input.isTouched()){
            tamaño=aux_rot-Gdx.input.getX();
            /*System.out.print(Gdx.input.getX());
            System.out.println();
            System.out.print(Gdx.input.getY());
            System.out.println();*/
        }
        else
            aux_rot=Gdx.input.getX()+tamaño;

        if (Gdx.input.isKeyPressed(Input.Keys.X)) {
            game.setScreen(new GameOver(game));
        }
    }

    private class Box2DScreenContactListener implements ContactListener {
        @Override
        public void beginContact(Contact contact) {
            Fixture fixtureA = contact.getFixtureA(), fixtureB = contact.getFixtureB();
            if (fixtureA.getUserData() == null || fixtureB.getUserData() == null) {
                return;
            }
        }

        @Override
        public void endContact(Contact contact) {
            Fixture fixtureA = contact.getFixtureA(), fixtureB = contact.getFixtureB();

        }
        @Override public void preSolve(Contact contact, Manifold oldManifold) { }
        @Override public void postSolve(Contact contact, ContactImpulse impulse) { }
    }
}
