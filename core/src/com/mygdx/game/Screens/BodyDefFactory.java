package com.mygdx.game.Screens;

import com.badlogic.gdx.physics.box2d.BodyDef;

public class BodyDefFactory {public static BodyDef createPlayer() {
    BodyDef def = new BodyDef();
    def.position.set(0, 0);
    def.type = BodyDef.BodyType.DynamicBody;
    return def;
}

    public static BodyDef createWall(float x, float y) {
        BodyDef def = new BodyDef();
        def.position.set(x, y);
        return def;
    }

    public static BodyDef createFloor() {
        BodyDef def = new BodyDef();
        def.position.set(0,0);
        return def;
    }
}