package com.mygdx.game.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.mygdx.game.Actors.But_n;
import com.mygdx.game.Actors.ClickEventHandler;
import com.mygdx.game.MyGdxGame;

public class Menu extends BaseScreen {

    SpriteBatch batch;
    private Stage stage;
    private Texture floor;
    private But_n button;
    private Texture Texture_button;

    public Menu(MyGdxGame game) {

        super(game);
        stage= new Stage();
        batch = new SpriteBatch();
        floor= new Texture("15.png");
        stage = new Stage(new FitViewport(640, 360));
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
        Texture_button=new Texture("playnow.png");
        button = new But_n(Texture_button,10,10,0.5f,0.5f);

        button.onClick(new ClickEventHandler() {
            @Override
            public void handleClick() {
                button.dead();
                game.setScreen(new Box2DScreen(game));
            }
        });

        stage.addActor(button);
    }
    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0.9f, 0.85f, 0.85f, 1f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stage.act();
        batch.begin();
        batch.draw(floor, 0f, 0f,1600,900);
        batch.end();
        stage.draw();
    }
    @Override
    public void hide() {
    }

    public void dispose () {
        batch.dispose();
        stage.dispose();
    }

}
