package com.mygdx.game.Screens;

import com.badlogic.gdx.graphics.g3d.utils.shapebuilders.SphereShapeBuilder;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.PolygonShape;

public class FixtureFactory {
   public static Fixture createPlayerFixture(Body playerBody){
        //PolygonShape CharacterShape = new PolygonShape();
        CircleShape CharacterShape=new CircleShape();
        CharacterShape.setRadius(0.58f);
        //CharacterShape.setAsBox(0.5f, 0.5f);
        Fixture fixture = playerBody.createFixture(CharacterShape, 3);
        CharacterShape.dispose();
        return fixture;
}

    public static Fixture createEnemyFixture(Body EnemyBody) {
        /*PolygonShape box = new PolygonShape();
        box.setAsBox(500, 1);
        Fixture fixture = floorBody.createFixture(box, 1);
        box.dispose();
        return fixture;*/
        CircleShape CharacterShape=new CircleShape();
        CharacterShape.setRadius(0.58f);
        //CharacterShape.setAsBox(0.5f, 0.5f);
        Fixture fixture = EnemyBody.createFixture(CharacterShape, 3);
        CharacterShape.dispose();
        return fixture;

    }

    public static Fixture createWallFixture(Body WallBody) {
        // This is the harder shape because it is not a box. I have to design the shape in
        // terms of vertices. So I create a vertex array to give the shape all the vertices.
        PolygonShape CharacterShape = new PolygonShape();
        CharacterShape.setAsBox(0.6f, 0.6f);
        Fixture fixture = WallBody.createFixture(CharacterShape, 3);
        CharacterShape.dispose();
        return fixture;

    }
}